package trash;

public class ClassA  extends ClassD<ClassB>{

    Long idA;

    Integer telefono;
    Integer mobil;
    String direccion;
    String color;

    public ClassA(Long idA, Integer telefono, Integer mobil, String direccion, String color) {
        this.idA = idA;
        this.telefono = telefono;
        this.mobil = mobil;
        this.direccion = direccion;
        this.color = color;
    }

    public Long getIdA() {
        return idA;
    }

    public void setIdA(Long idA) {
        this.idA = idA;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public Integer getMobil() {
        return mobil;
    }

    public void setMobil(Integer mobil) {
        this.mobil = mobil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
