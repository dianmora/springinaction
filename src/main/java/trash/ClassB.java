package trash;

public class ClassB  extends ClassC{

    Integer id;
    String name;
    String apellido;
    String edad;

    public ClassB(Integer id, String name, String apellido, String edad) {
        super(id, name, apellido, edad);
        this.id = id;
        this.name = name;
        this.apellido = apellido;
        this.edad = edad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }
}
