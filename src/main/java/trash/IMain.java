package trash;

import java.util.List;

public interface IMain {
    List<ClassA> retrieveClassAObject();
    List<ClassB> retrieveClassBObject();
    <T> List<T> retrieveGenericClass(Class<T> clas);
    <T> List<T> retrieveGenericClass();

}
