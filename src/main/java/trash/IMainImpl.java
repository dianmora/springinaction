package trash;

import java.util.Arrays;
import java.util.List;

public class IMainImpl implements IMain {

    public List<ClassA> retrieveClassAObject()  {

        ClassA aa = new ClassA(1L, 5, 7,"direccion","color");
        return retrieveGenericClass(ClassA.class);
    }


    public List<ClassB> retrieveClassBObject() {
        ClassB b = new ClassB(1, "classB name", "classBppellido", "15");
       return retrieveGenericClass(ClassB.class);
    }

    @Override
    //public <T> List<T> retrieveGenericClass(Class<T> clas) {
    public <T> List<T> retrieveGenericClass(Class<T> clas) {
        String msg ="";
        Class<T> caseT;
        //boolean b = caseT instanceof trash.ClassA;
        boolean isClassB = clas.isAssignableFrom(ClassB.class);
        boolean isClassA = clas.isAssignableFrom    (ClassA.class);

        if(isClassA){
            ClassA aa = new ClassA(1L, 5, 7,"direccion","color");
            return (List<T>) Arrays.asList(aa);
        }
        else{
            ClassB b = new ClassB(1, "classB name", "classBAppellido", "15");
            return (List<T>) Arrays.asList(b);

        }


    }

    @Override
    public <T> List<T> retrieveGenericClass() {

        return null;
    }

}
